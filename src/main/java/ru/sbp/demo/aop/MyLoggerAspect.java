package ru.sbp.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.logging.Logger;

@Component
@Aspect
public class MyLoggerAspect {
    private Logger logger = Logger.getLogger(MyLoggerAspect.class.getName());

    /**
     * Точка среза - ContributionsController
     */
    @Pointcut("within(ru.sbp.demo.services.ContributionsController)")
    private void myLogPointCutController() {

    }

    /**
     * Метод логирует в консоль успешный старт микросервиса
     *
     * @param joinPoint
     */
    @Before("myLogPointCutController()")
    public void startLog(JoinPoint joinPoint) {
        logger.info("Start controller service " + joinPoint.getSignature().getName() + " " + LocalDateTime.now());
    }

    /**
     * Метод логирует в консоль успешный финиш работы микросервиса
     *
     * @param joinPoint
     */
    @After("myLogPointCutController()")
    public void finishLog(JoinPoint joinPoint) {
        logger.info("Finish controller service " + joinPoint.getSignature().getName() + " " + LocalDateTime.now());
    }

    /**
     * Точка среза - ContributionsDAOService
     */
    @Pointcut("within(ru.sbp.demo.services.ContributionsDAOService)")
    private void myLogPointCutDAO() {

    }

    /**
     * Метод логирует в консоль успешный старт работы микросервиса
     *
     * @param joinPoint
     */
    @Before("myLogPointCutDAO()")
    public void startLogDAO(JoinPoint joinPoint) {
        logger.info("Start DAO service " + joinPoint.getSignature().getName() + " " + LocalDateTime.now());
    }

    /**
     * Метод логирует в консоль успешный финиш работы микросервиса
     *
     * @param joinPoint
     */
    @After("myLogPointCutDAO()")
    public void finishLogDAO(JoinPoint joinPoint) {
        logger.info("Finish DAO service " + joinPoint.getSignature().getName() + " " + LocalDateTime.now());
    }
}
