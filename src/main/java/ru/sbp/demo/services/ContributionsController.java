package ru.sbp.demo.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sbp.demo.pojo.Contribution;


@Controller
@RequestMapping("/contribution")
public class ContributionsController {

    private final String titleTableContrib = "contributions";

    private ContributionsDAOService contributionsDAOService;

    @Autowired
    public ContributionsController(ContributionsDAOService contributionsDAOService) {
        this.contributionsDAOService = contributionsDAOService;
    }

    /**
     * Возвращает HTML страницу с приветствием и просьбой выбрать операцию
     */
    @GetMapping
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("/contribution/index");
        return modelAndView;
    }

    /**
     * Возвращает HTML страницу с формой создания вклада. В форме имя продукта и первоначальный взнос
     */
    @GetMapping("/new")
    public ModelAndView newContribution() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("contribution", new Contribution());
        modelAndView.setViewName("/contribution/new");
        return modelAndView;
    }

    /**
     * Возвращает страницу HTML с информацией о успешности создания вклада, и сведения о созданном вкладе.
     *
     * @param contribution
     * @return
     */
    @PostMapping()
    public ModelAndView create(@ModelAttribute("contribution") Contribution contribution) {
        ModelAndView modelAndView = new ModelAndView();
        contributionsDAOService.addContribution(contribution, titleTableContrib);
        modelAndView.setViewName("/contribution/created");
        return modelAndView;
    }

    /**
     * Отображает перечень всех вкладов с данными о балансе
     *
     * @return
     */
    @GetMapping("/show")
    public ModelAndView checkContribution() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("contribution", contributionsDAOService.findAll(titleTableContrib));
        modelAndView.setViewName("/contribution/show");
        return modelAndView;
    }

    /**
     * Возвращает HTML страницу с формой создания вклада
     */
    @GetMapping("/showDelete")
    public ModelAndView delContribution() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("contribution", contributionsDAOService.findAll(titleTableContrib));
        modelAndView.addObject("contrib", newContribution());
        modelAndView.setViewName("/contribution/showDelete");
        return modelAndView;
    }

    /**
     * Метод представляет сылочный список всех вкладов.
     */
    @GetMapping("/{name}")
    public ModelAndView show(@PathVariable("name") String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("contribution", contributionsDAOService.findByNameProduct(name, titleTableContrib));
        modelAndView.setViewName("contribution/delete");
        return modelAndView;
    }

    /**
     * Метод удаляет выбранный по имени продукта вклад.
     * Возвращает страницу HTML со списком возможных операций.
     */
    @GetMapping("/delete/{name}")
    public String delete(@PathVariable("name") String name) {
        contributionsDAOService.deleteByName(name, titleTableContrib);
        return "redirect:/contribution";
    }
}
