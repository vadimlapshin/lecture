package ru.sbp.demo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;
import ru.sbp.demo.pojo.Contribution;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


@Repository
@PropertySource("classpath:db.properties")
public class ContributionsDAOService {

    private String url;

    @Value("${url}")
    public void setUrl(String url) {
        this.url = url;
    }

    public ContributionsDAOService() {
    }

    /**
     * Метод добавляет объект типа Contribution в таблицу БД сontributions.
     *
     * @param
     */
    public synchronized void addContribution(Contribution contribution, String titleTable) {
        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String sql = String.format("insert into " + titleTable + " ('nameProduct', 'balance') VALUES ('%s', '%s')",
                    contribution.getNameProduct(), contribution.getBalance());
            statement.execute(sql);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Метод поиска значения баланса в таблице базы данных по полю nameProduct. Возвращает объект Contribution.
     *
     * @param
     * @return
     */
    public synchronized Contribution findByNameProduct(String nameProduct, String titleTable) {
        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String strSql = "select * from " + titleTable + " where nameProduct = '" + nameProduct + "'";
            boolean hasResultSet = statement.execute(strSql);
            ResultSet resultSet = statement.getResultSet();
            if (!resultSet.next()) return null;
            String nameProduct1 = resultSet.getString("nameProduct");
            double balance = resultSet.getDouble("balance");
            return new Contribution(nameProduct1, balance);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Метод удаления строки в таблице БД по заданному имени.
     *
     * @param
     */
    public synchronized void deleteByName(String nameProduct, String titleTable) {
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement("delete from " + titleTable + " where nameProduct = ?")) {
            preparedStatement.setString(1, nameProduct);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Метод поиска всех элементов таблицы. Возвращает в виде списка.
     *
     * @return
     */
    public synchronized List<Contribution> findAll(String titleTable) {
        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement()) {
            String strSql = "select * from " + titleTable;
            List<Contribution> listPerson = new LinkedList<>();
            ResultSet resultSet = statement.executeQuery(strSql);
            while (resultSet.next()) {
                String nameProduct = resultSet.getString("nameProduct");
                double balance = resultSet.getDouble("balance");
                Contribution contribution = new Contribution(nameProduct, balance);
                listPerson.add(contribution);
            }
            return listPerson;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
}
