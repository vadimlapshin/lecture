package ru.sbp.demo.pojo;

public class Contribution {

    private String nameProduct;
    private double balance;

    public Contribution() {
    }

    public Contribution(String nameProduct, double balance) {
        this.nameProduct = nameProduct;
        this.balance = balance;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public double getBalance() {
        return balance;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Contribution{" +
                "nameProduct='" + nameProduct + '\'' +
                ", balance=" + balance +
                '}';
    }
}
