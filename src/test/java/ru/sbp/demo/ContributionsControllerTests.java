package ru.sbp.demo;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.sbp.demo.pojo.Contribution;
import ru.sbp.demo.services.ContributionsController;
import ru.sbp.demo.services.ContributionsDAOService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ContributionsControllerTests {
    @Mock
    private ContributionsDAOService contributionsDAOService;

    @InjectMocks
    private ContributionsController contributionsController;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void before() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(contributionsController).build();
    }

    /**
     * Тест на успешность работы метода .index().
     */
    @Test
    public void index_test() throws Exception {
        mockMvc.perform(get("/contribution"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("/contribution/index"));
    }

    /**
     * Тест на успешность работы метода .newContribution().
     */
    @Test
    public void newContribution_test() throws Exception {
        mockMvc.perform(get("/contribution/new"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("/contribution/new"))
                .andExpect(MockMvcResultMatchers.model().attribute("contribution", instanceOf(Contribution.class)));
    }

    /**
     * Тест на успешность работы метода .create().
     */
    @Test
    public void create_test() throws Exception {
        Mockito.verifyNoInteractions(contributionsDAOService);
        mockMvc.perform(post("/contribution"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("/contribution/created"));
    }

    /**
     * Тест на успешность работы метода .checkContribution.
     */
    @Test
    public void checkContribution_test() throws Exception {
        Mockito.verifyNoInteractions(contributionsDAOService);
        List<Contribution> contributionList = new ArrayList<>();
        Mockito.when(contributionsDAOService.findAll("contributions")).thenReturn(contributionList);
        mockMvc.perform(get("/contribution/show"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("/contribution/show"));
    }

    /**
     * Тест на успешность работы метода .delContribution.
     */
    @Test
    public void delContribution_test() throws Exception {
        Mockito.verifyNoInteractions(contributionsDAOService);
        List<Contribution> contributionList = new ArrayList<>();
        Mockito.when(contributionsDAOService.findAll("contributions")).thenReturn(contributionList);
        mockMvc.perform(get("/contribution/showDelete"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("/contribution/showDelete"));
    }
}
